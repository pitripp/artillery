

## To run with Spring Boot:

Run `mvn spring-boot:run`. Navigate to `http://localhost:5000/`

## Build a jar file that can be put to a server

Run `./mvnw clean package`

I used Java SDK and JRE 1.8.191 on Mac OS X by changing mvnw wrapper script

export JAVA_HOME=`/usr/libexec/java_home -v 1.8.0_191`

It also works using v 11. using the default

`mvn clean package`

## Test jar
java -jar <app_version>.jar

## Development server - test the front end

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. You will see the Angular application running in the browser.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
