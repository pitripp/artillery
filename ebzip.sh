#!/bin/ksh

version=1.0.2
tmpdir=./tmpdir

rm -Rf $tmpdir
mkdir -p $tmpdir

cp -p target/*.jar $tmpdir
cp -rp .ebextensions $tmpdir

cd $tmpdir
zip -r artillery_deploy_v${version}.zip *.jar .ebextensions/


