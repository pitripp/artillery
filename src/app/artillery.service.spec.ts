import { TestBed } from '@angular/core/testing';

import { ArtilleryService } from './artillery.service';

describe('ArtilleryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArtilleryService = TestBed.get(ArtilleryService);
    expect(service).toBeTruthy();
  });
});
