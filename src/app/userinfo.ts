
// Object for JSON serialization from server

/**
 * User stats and shot result from backend
 */
export class UserInfo { 
    
    username : string;
    shotcount : number;
    hitcount : number;
    average : number;
    rank : number;
    lastStrikeDistance : number;
}
