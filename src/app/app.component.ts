import { Component, OnInit, NgModule } from '@angular/core';
import * as constants from './constants';
import { Stats } from './stats';
import { UserInfo } from './userinfo';
import { ArtilleryService } from './artillery.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

@NgModule({
  declarations: [
  ],
  imports: [
  ]
})

/** 
 * The main app component
 */
export class AppComponent implements OnInit {
  
  title = 'Artillery';

  VELOCITIES : number[];
  GRAVITIES : any[];

  shell_weight : number;
  target_distance : number;
  target_size : number;

  username : string;
  browser : string;

  barrel_angle : number;
  muzzle_velocity : number;
  gravity : number;

  topUsers : UserInfo[];
  globalstats : Stats;

  // these will be defined after first shot
  userinfo : UserInfo;
  shotresult: string;
  lastStrikeDistance: number;

  errmsg : string;

  readyToFire : boolean;

  /**
   * 
   * @param artilleryService the http client to the backend
   */
  constructor(public artilleryService: ArtilleryService ) {
    this.VELOCITIES=constants.MUZZLE_VELOCITIES;
    this.GRAVITIES=constants.GRAVITIES;
  }


  /**
   * Initialization routine, called whenever entering/loading the page
   */
  ngOnInit() {

    this.readyToFire = false;
    this.browser = navigator.appVersion;
    
    this.getStats();
    this.randomShell();
    this.randomTarget();

    this.barrel_angle = 45.0;
    this.muzzle_velocity = this.VELOCITIES[0];
    this.gravity = this.GRAVITIES[0].gravity;
    this.shotresult = "";
  }


  /**
   * Require a username to enable the fire button
   * TODO: Better validation 
   */
  onUsernameInput() {
    this.readyToFire = this.username.length > 1;
  }


  randomTarget() {
    this.target_distance = this.randomInteger(constants.TARGET_DISTANCE_MIN, constants.TARGET_DISTANCE_MAX);
    this.target_size = this.randomInteger(constants.TARGET_SIZE_MIN, constants.TARGET_SIZE_MAX);
  }


  /**
   * Obtains a random shell weight from the list
   */
  randomShell() {
    let index = Math.floor(Math.random() * constants.SHELL_WEIGHTS.length );
    this.shell_weight = constants.SHELL_WEIGHTS[index];
  }

  
  randomInteger(min, max) : number {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
  }


  /**
   * Event triggered when clicking the Fire button.
   * Calls the backend api to do the calculations and return the results
   */
  onFire() {

    this.artilleryService.getShotResults(this.username, this.browser, this.shell_weight, this.muzzle_velocity,
      this.barrel_angle, this.gravity, this.target_size, this.target_distance)
      .subscribe(
        data => {
        this.userinfo = data;
          if (data.lastStrikeDistance == 0) {
            this.shotresult = "HIT!"

            // Reset target
            this.randomShell();
            this.randomTarget();
          }
          else {
            if (data.lastStrikeDistance < 0)
              this.shotresult = "Undershot target by " + Math.abs(data.lastStrikeDistance).toFixed(0) + " meters";
            else this.shotresult = "Overshot target by " + data.lastStrikeDistance.toFixed(0) + " meters";
          }
          this.getStats();

          // Hack extreme/undefined rank for prettier display
          if (this.userinfo.rank == constants.MAXINT) this.userinfo.rank = 0;
          if (this.userinfo.average >= constants.MAXFLOAT) this.userinfo.average = 0.0;
        },
        err => this.errmsg = err
      )

  }

  /**
   * Retrieves the current stats from the backend
   */
  getStats() {

    this.artilleryService.getTopUsers().subscribe(
      data => {
      this.topUsers = data;

        // Hack extreme/undefined rank and average for prettier display
        for (let user of this.topUsers) {
          if (user.rank == constants.MAXINT) user.rank = 0;
          if (user.average >= constants.MAXFLOAT) user.average = 0.0;
        }

        this.artilleryService.getStats().subscribe(
          data => this.globalstats = data,
          err => this.errmsg = err
        )
      },
      err => this.errmsg = err
    )

  }
}

