import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { Observable, throwError } from 'rxjs';
import { UserInfo } from './userinfo';
import { Stats } from './stats';

@Injectable({
  providedIn: 'root'
})
export class ArtilleryService {

  constructor(private http: HttpClient) { }



  getTopUsers(): Observable<UserInfo[]> {
    return this.http.get<UserInfo[]>('/api/topusers')
      .pipe(catchError(this.handleError));
  }



  getStats(): Observable<Stats> {
    return this.http.get<Stats>('/api/stats')
      .pipe(catchError(this.handleError));
  }



  getShotResults(username: string, browser: string, mass: number, velocity: number, angle: number,
    gravity: number, targetsize: number, targetdistance: number): Observable<UserInfo> {

    let qstring = '/api/shot?username=' + username + "&browser=" + browser + "&mass=" + mass + "&velocity=" + velocity + "&angle=" + angle +
      "&gravity=" + gravity + "&targetSize=" + targetsize + "&targetDistance=" + targetdistance;



    return this.http.get<UserInfo>('/api/shot?username=' + username + "&browser=" + browser + "&mass=" + mass + "&velocity=" + velocity + "&angle=" + angle +
      "&gravity=" + gravity + "&targetSize=" + targetsize + "&targetDistance=" + targetdistance)
      .pipe(catchError(this.handleError));
  }



  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

}