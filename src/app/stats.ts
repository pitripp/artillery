/**
 * Global stats object
 */
export class Stats {
    shotcount : number;
    hitcount : number;
    usercount : number;
    average : number;
}