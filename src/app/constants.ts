
// Rank returned by backend when # hits = 0
export const MAXINT : number =  2147483647;

// Average returned by backend when # hits = 0 
export const MAXFLOAT : number = 3.4e+38;

// Assumption: Projectile weight == shell weight
// kg
export const SHELL_WEIGHTS: number[] = [
    5.98, 6.04, 7, 10, 13.6, 14.4, 14.6, 15.2, 15.5, 16, 16.23, 16.3, 17.45, 17.7, 18, 18, 18.1, 19.05, 19.47, 19.47, 
    19.9, 20.1, 21, 21.06, 21.42, 22.25, 23, 23.17, 25, 25, 26, 26.1, 26.6, 27, 28.5, 28.5, 28.5, 30, 30.22, 40, 
    42.84, 43, 43.49, 43.88, 44.4, 44.68, 44.68, 45.3, 45.34, 45.4, 45.5, 46.26, 46.5, 46.54, 46.54, 46.75, 47, 47.3, 
    47.5, 47.77, 47.92, 48.25, 48.27, 49.15, 49.15, 49.5, 50.89, 51.1, 51.15, 51.19, 53, 54.49, 54.8, 54.92, 55, 55, 
    55.52, 55.79, 56, 56.38, 57.27, 58.4, 58.67, 58.94, 59, 59.1, 59.1, 59.5, 59.57, 60.73, 60.73, 60.73, 62.6, 62.93,
    82, 86, 87.57, 88, 88, 88.86, 96, 97.69, 97.75, 97.75, 99.19, 102.28, 104.51, 105.6, 107.53, 153, 155
]

// meters
export const TARGET_DISTANCE_MIN: number = 1500;
export const TARGET_DISTANCE_MAX: number = 70000;

// smaller targets are harder to hit
export const TARGET_SIZE_MIN: number = 200;
export const TARGET_SIZE_MAX: number = 500;


// meters/second
export const MUZZLE_VELOCITIES : number[] = [
    472, 494, 518, 563, 670, 680, 770, 827, 900 
]

// degrees
export const BARREL_ANGLE_MIN = 0.0;
export const BARREL_ANGLE_MAX = 90.0;


// meters/second^2
export const GRAVITIES : any = [
    {planet: "EARTH", gravity: 9.807},
    {planet: "MOON", gravity: 1.62},
    {planet: "MARS", gravity: 3.711},
    {planet: "SATURN", gravity: 10.44},
    {planet: "MERCURY", gravity: 3.7},
    {planet: "JUPITER", gravity: 24.79},
    {planet: " NEPTUNE", gravity: 11.15},
    {planet: "VENUS", gravity: 8.87},
    {planet: "URANUS", gravity: 8.87},
    {planet: "PLUTO", gravity: 0.62},
    {planet: "SUN", gravity: 274}
];

