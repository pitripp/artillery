package com.patrick_tripp.artillery;

import com.patrick_tripp.artillery.userdata.UserInfo;
import com.patrick_tripp.artillery.userdata.UserService;

import javax.servlet.http.HttpServletRequest;

import com.patrick_tripp.artillery.statsdata.StatsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Main REST controller 
 */
@RestController
public class ArtilleryController {

    // Service layer for UserRepository
    @Autowired 
    private UserService userService;


    // Service layer for StatsRepository
    @Autowired
    private StatsService statsService;


    public ArtilleryController() {
        userService = new UserService();
        statsService = new StatsService();
    }


    @GetMapping(path="/api/shot")
    /**
     * 
     * Updates and returns {@link UserInfo} after results of this shot
     * 
     * @param username
     * @param browser
     * @param mass
     * @param velocity
     * @param angle
     * @param gravity
     * @param targetSize
     * @param targetDistance
     * 
     * @return updated {@link UserInfo}
     * 
     * Example: http://localhost:5000/api/shot?username=user9&browser=Chrome/74.0.3729.169 Safari/537.36&mass=10.0&velocity=472&angle=88&gravity=9.8&targetSize=10&targetDistance=1500
     */
    public UserInfo shotResult( @RequestParam String username, @RequestParam String browser,
                                @RequestParam Double mass, @RequestParam Double velocity, @RequestParam Double angle, 
                                @RequestParam Double gravity, @RequestParam Integer targetSize, 
                                @RequestParam Integer targetDistance, HttpServletRequest request ) 
    {

        // TODO - data validation, error check

        // Get current user info or add new user if new
        UserInfo userinfo = null;

        if ( userService.userExists(username) ) {            
            userinfo = userService.getUser(username);
        } 
        else {
            String ipAddress = request.getRemoteAddr();
            UserInfo newuser = new UserInfo(username, ipAddress, browser); // (username, ipAddress, browser)); 
            userinfo = userService.addUser(newuser);
            statsService.incrementUsers();
        }
        

        // Call to compute the trajectory
        Trajectory trajectory = new Trajectory(mass, velocity, angle, gravity, targetSize, targetDistance);

        // Update and save the stats
        userinfo.addTrajectory(trajectory);
        userService.save(userinfo);
        statsService.incrementShots();

        /*
        // update rank only after a hit
        if (userinfo.getLastStrikeDistance() == 0.0) {
            userService.updateRanks();
            statsService.incrementHits();
            // get updated userinfo, rank might have changed
            userinfo = userService.getUser(username);
        }
        */

        // update ranks on misses also
        if (userinfo.getLastStrikeDistance() == 0.0) {
            statsService.incrementHits();
        }
        userService.updateRanks();
        userinfo = userService.getUser(username);

        return userinfo;
    }       
       
}

