package com.patrick_tripp.artillery;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Projectile trajectory with the provided parameters and target
 * 
 * @author Patrick Tripp
 */
@Entity
public class Trajectory {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private Double mass; 
    private Double velocity;
    private Double angle;
    private Double gravity;
        
    private Integer targetDistance;
    private Integer targetSize;

    // Distance the ballistic travelled 
    private Double range;

    // Landing distance from target
    private Double strikeDistance;

    // DB joined to user
    private Long user_id;

    
    public Trajectory() {

    }


    /**
     * Create a new Trajectory instance with all needed parameters
     * The range is computed here also, setting the final trajectory range and distance from target
     * 
     * @param mass kg
     * @param velocity m/s
     * @param angle degrees
     * @param gravity m/s^2
     * @param targetSize m
     * @param targetDistance m
     */
    public Trajectory(Double mass, Double velocity, Double angle, Double gravity, 
    Integer targetSize, Integer targetDistance ) {

        this.mass = mass;
        this.velocity = velocity;
        this.angle = angle;
        this.gravity = gravity;
        this.targetSize = targetSize;
        this.targetDistance = targetDistance;

        computeTrajectory();
    }
    



    /*
     * Compute the range of the fired projectile 
     * Compute the strike distance from target 
     * 
     * strikeDistance = 0 if hit was within target zone
     * negative distance = short from strikezone edge
     * positive distance = long of strikezone edge
     * 
     * Ignoring wind and air resistance for now
    */
	private void computeTrajectory() {

        /*
        // mass does not matter 
        Using equation: d = (v^2 sin(2*theta))/g
        */
        Double theta = angle * Math.PI/180.0;

        range =  (Math.pow(velocity, 2) * Math.sin(2*theta)) / gravity;   

        double mintarget = targetDistance - (0.5 * targetSize);  // 95 
        double maxtarget = targetDistance + (0.5 * targetSize);  // 105
        if ( range <= maxtarget && range >= mintarget ) strikeDistance = 0.0;
        else strikeDistance = range - targetDistance;
    }


    /**
     * 
     * @return
     */
    public Double getStrikeDistance() {
        return strikeDistance;
    }


    /* getters and setters */

    public Long getId() {
        return id;
    }
    
    public Long getUser_id() {
        return user_id;
    }

    public Double getMass() {
        return mass;
    }


    public Double getVelocity() {
        return velocity;
    }

    public Double getAngle() {
        return angle;
    }

    public Double getGravity() {
        return gravity;
    }

    public Integer getTargetDistance() {
        return targetDistance;
    }

    public Integer getTargetSize() {
        return targetSize;
    }

    public Double getRange() {
        return range;
    }



}