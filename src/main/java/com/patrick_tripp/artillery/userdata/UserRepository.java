package com.patrick_tripp.artillery.userdata;

import java.util.List;

import com.patrick_tripp.artillery.Trajectory;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * User data reposittory
 */
public interface UserRepository  extends CrudRepository<UserInfo, Long> {
    
    /**
     * 
     * @param username
     * @return
     */
    UserInfo findByUsername ( String username );
    

    /**
     * 
     * @param username
     * @return
     */
    boolean existsByUsername ( String username );


    /**
     * 
     * @return
     */
    List<UserInfo> findAllByOrderByAverageAsc();


    /**
     * 
     * @return
     */
    List<UserInfo> findTop5ByOrderByRankAsc();

    /** 
     * 
     * @return
     */
    List<UserInfo> findAll();

    /**
     * 
     * @return
     */
    @Query("SELECT t FROM Trajectory t")
    List<Trajectory> findAllTrajectories();

}