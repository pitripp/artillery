package com.patrick_tripp.artillery.userdata;

import com.patrick_tripp.artillery.Trajectory;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;



/**
 * Object containing the stats and info for each user
 * Is also a DB entity
 * 
 * @author Patrick Tripp
 */
@Entity
public class UserInfo {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    private String username;
    private String ipAddress;
    private String browser;

    private Integer shotcount;
    private Integer hitcount;

    // Average # of shots to hit target
    private Float average;
    private Integer rank;


    // Most recent shot landing distance from target
    private Double lastStrikeDistance;

    
    @OneToMany(orphanRemoval=true, cascade = CascadeType.ALL) 
    @JoinColumn(name="USER_ID")
    private List<Trajectory> trajectories;
        

    public UserInfo() {
    }


    /**
     * Creates a new UserInfo instance
     * 
     * @param username
     * @param ipAddress
     * @param browser
     */
    public UserInfo(String username, String ipAddress, String browser) {
        this.username = username;
        this.ipAddress = ipAddress;
        this.browser = browser;
        this.hitcount=0;
        shotcount=0;
        average=0.0f;
        rank = Integer.MAX_VALUE;

        lastStrikeDistance = Double.MIN_VALUE;
        trajectories = new ArrayList<Trajectory>();   
    }


    /**
     * Adds a {@link Trajectory} to this object
     * 
     * @param trajectory
     * @return boolean result of {@link List} add
     */

    public boolean addTrajectory(Trajectory trajectory) {
        boolean result = trajectories.add(trajectory);
        updateStats(trajectory);
        return result;
    }


    /**
     * Update this UserInfo with the provided trajectory
     * 
     * @param trajectory
     */
    private void updateStats(Trajectory trajectory) {
        lastStrikeDistance = trajectory.getStrikeDistance();
        if (lastStrikeDistance == 0.0 ) {
            this.hitcount = this.hitcount + 1;
        }
        shotcount++;
        if (hitcount == 0) average = Float.MAX_VALUE;
        else average = (float) shotcount/ (float) hitcount;
    }



    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserInfo other = (UserInfo) obj;
        if (username == null) {
            if (other.username != null)
                return false;
        } else if (!username.equals(other.username))
            return false;
        return true;
    }


    /* auto generated getters */
    public String getUsername() {
        return username;
    }

    public Integer getShotcount() {
        return shotcount;
    }

    public Integer getHitcount() {
        return hitcount;
    }

    public Float getAverage() {
        return average;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Double getLastStrikeDistance() {
        return lastStrikeDistance;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getBrowser() {
        return browser;
    }





}