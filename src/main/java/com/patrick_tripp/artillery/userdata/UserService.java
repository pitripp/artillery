package com.patrick_tripp.artillery.userdata;

import java.util.List;

import com.patrick_tripp.artillery.Trajectory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service layer to {@link UserRepository}
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userDB;

    /**
     * @param username
     * @return
     */
    public boolean userExists(String username) {
        return userDB.existsByUsername(username);
    }

    /**
     * Add a new {@link UserInfo} to the database
     * 
     * @param user
     * @return the {@link UserInfo} added to DB
     */
    public UserInfo addUser(UserInfo user) {

        return userDB.save(user);
    }


    /**
     * Returns the UserInfo for a pecified username
     * 
     * @param username
     * @return 
     */
    public UserInfo getUser(String username) {
        return userDB.findByUsername(username);
    }


    /**
     * Updates a {@link UserInfo}
     *  
     * @param user
     * @return the {@link UserInfo} updated in DB
     */
    public UserInfo save(UserInfo user) {
        return userDB.save(user);
    }

    /**
     * Updates the rank for every user
     * ranks are tied if more than one user has the same average
     */
    public void updateRanks() {
       List<UserInfo> allUsers = userDB.findAllByOrderByAverageAsc();
       int usercount = allUsers.size();
       int prevrank = 1;
       float prevavg = 0.0f;

       
       for (int i = 0; i < usercount; i++) {
        
            UserInfo user = allUsers.get(i);

            // no rank if no hits
            if (user.getHitcount() == 0 ) {
                user.setRank(Integer.MAX_VALUE);
                continue;
            }

            float average = user.getAverage();
            if (average == prevavg) user.setRank(prevrank);
            else {
                user.setRank(i+1);
                prevrank = i+1;
                prevavg = average;
            }
            userDB.save(user);
       }
    }

    /**
     * Retrieves the top five ranked users from the database
     * 
     * @return 
     */
    public List<UserInfo> getTopUsers() {

        return userDB.findTop5ByOrderByRankAsc();
    }

    /**
     * Retrieves all users
     * @return
     */
    public List<UserInfo> getAllUsers() {
        return userDB.findAll();
    }

    /**
     * Retrieve all Trajectories from DB
     * @return
     */
    public List<Trajectory> getAllTrajectories() {
        return userDB.findAllTrajectories();
    }


}