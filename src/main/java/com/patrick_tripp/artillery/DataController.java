package com.patrick_tripp.artillery;

import java.util.List;

import com.patrick_tripp.artillery.statsdata.Stats;
import com.patrick_tripp.artillery.statsdata.StatsService;
import com.patrick_tripp.artillery.userdata.UserInfo;
import com.patrick_tripp.artillery.userdata.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/** 
 * REST controller for global stats 
 * 
 * */
@RestController
class DataController {

    @Autowired
    private StatsService statsService;

    @Autowired
    private UserService userService;

    /**
     * 
     */
    public DataController () {
        statsService = new StatsService();
        userService = new UserService();
    }


    /**
     * Global stats endpoint
     * 
     * @return
     */
    @GetMapping(path="/api/stats") 
    public Stats getStats() {
        return statsService.getStats();
    }

    /**
     * Top 5 Users endpoint
     * @return
     */
    @GetMapping(path="/api/topusers")
    public List<UserInfo> getTopUsers() {
        return userService.getTopUsers();
    }

    /**
     * All Users endpoint
     * @return
     */
    @GetMapping(path="/api/users")
    public List<UserInfo> getUsers() {
        return userService.getAllUsers();
    }

    /**
     * All trajectories endpoint
     * @return
     */
    
    @GetMapping(path="/api/trajectories") 
    public List<Trajectory> getTrajectories() {
        return userService.getAllTrajectories();
    }
    
}