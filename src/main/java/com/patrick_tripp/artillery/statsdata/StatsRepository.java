package com.patrick_tripp.artillery.statsdata;

import org.springframework.data.repository.CrudRepository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Query;

/**
 * Global stats repository 
 */
public interface StatsRepository extends CrudRepository<Stats, Long> {

    /**
     * 
     * @return
     */
    @Modifying
    @Transactional
    @Query("UPDATE Stats s SET s.shotcount = s.shotcount + 1 WHERE s.id = 1")
    int incrementShotcount();

    /**
     * 
     * @return
     */
    @Modifying
    @Transactional
    @Query("UPDATE Stats s SET s.hitcount = s.hitcount + 1 WHERE s.id = 1") 
    int incrementHitcount();

    /**
     * 
     * @return
     */
    @Modifying
    @Transactional
    @Query("UPDATE Stats s SET s.usercount = s.usercount + 1 WHERE s.id = 1")
    int incrementUsercount();

    /**
     * 
     * Update global average shots/hits
     * Note: this will throw an exception if hits = 0 
     *  - make sure DB is properly initialized after recreating schema
     * 
     * INSERT INTO stats (id, average, hitcount, shotcount, usercount) VALUES (1,0.0,0,1,0)
     * 
     * @return
     */
    @Modifying
    @Transactional
    @Query("UPDATE Stats s SET s.average = cast(s.shotcount as double)/cast(s.hitcount as double) WHERE s.id = 1")
    int updateAverage();

    
}