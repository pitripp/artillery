package com.patrick_tripp.artillery.statsdata;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Global cumulative statistics for all users
 * 
 * @author Patrick Tripp
 */
@Entity
public class Stats {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private Long shotcount;
    private Long hitcount;
    private Long usercount;
    private Float average;

    public Stats() {
    }

    public Long getShotcount() {
        return shotcount;
    }

    public Long getHitcount() {
        return hitcount;
    }

    public Long getUsercount() {
        return usercount;
    }

    public Float getAverage() {
        return average;
    }

}