package com.patrick_tripp.artillery.statsdata;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service layer for global stats {@link StatsRepository}
 */
@Service
public class StatsService {

    @Autowired
    private StatsRepository statsDB;

    /**
     * 
     */
    public void incrementShots() {
        statsDB.incrementShotcount();
        statsDB.updateAverage();
    }

    /**
     * 
     */
    public void incrementHits() {
        statsDB.incrementHitcount();
        statsDB.updateAverage();
    }

    /**
     * 
     */
    public void incrementUsers() {
        statsDB.incrementUsercount();
    }

    /**
     * 
     * @return Stats - may be null if DB is not initialized
     * @throws NoSuchElementException
     */
    public Stats getStats() throws NoSuchElementException {
        
        final Long ONLYONE = 1l;
        
        Stats stats;
        try {
            stats = statsDB.findById(ONLYONE).get();
        } 
        catch ( NoSuchElementException e ) {
            throw(e);
        }
        
        return stats;
    }

}